﻿using UnityEngine;

namespace Assets.PlayerCharacter
{
    public class CursorMovement : MonoBehaviour
    {
        public float Speed;
        public float MaxDistance;

        private Cursor cursor;
        public bool IsEnabled { get; private set; }

        public void Initialize(Cursor cursor)
        {
            this.cursor = cursor;
            cursor.Initialize(MaxDistance);
        }

        public void Move(Vector2 inputVector)
        {
            if (IsEnabled)
            {
                var movementVector = new Vector3(inputVector.x, 0f, inputVector.y) * Speed * Time.deltaTime;
                cursor.MoveTo(movementVector);
            }
        }

        public void Enable()
        {
            cursor.Show();
            IsEnabled = true;
        }

        public void Disable()
        {
            cursor.Hide();
            IsEnabled = false;
        }
    }
}
