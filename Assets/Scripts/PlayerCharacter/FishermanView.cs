﻿using UnityEngine;

namespace Assets.PlayerCharacter
{
    public class FishermanView : MonoBehaviour
    {
        public Animator Animator;

        public void Drive() => Animator.SetTrigger("Drive");
        public void Aim() => Animator.SetTrigger("Aim");
        public void Cast() => Animator.SetTrigger("Cast");
    }
}
