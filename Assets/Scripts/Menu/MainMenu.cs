﻿using System.Collections;
using System.IO;
using Assets.Scripts.Common;
using Assets.Scripts.Player.Persistence;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Menu
{
    public class MainMenu : MonoBehaviour
    {
        public Animator Animator;
        public Fade Fade;
        public Selectable FirstMenuItem;
        public Selectable FirstOptionItem;

        public void SwitchToOptions()
        {
            FirstOptionItem.Select();
            Animator.SetTrigger("SwitchToOptions");
        }

        public void SwitchToMenu()
        {
            FirstMenuItem.Select();
            Animator.SetTrigger("SwitchToMain");
        }

        public void Exit() => Application.Quit(0);
        public void Play() => LoadLake();

        private void LoadLake()
        {
            Fade.FadeOut();
            StartCoroutine(WaitAndGoToLake());
        }

        public void ResetProfile()
        {
            PlayerPrefsHandler.Clear();
            FileSystem.SaveStats(new PlayerStats());
        }

        IEnumerator WaitAndGoToLake()
        {
            yield return new WaitForSeconds(1.5f);
            SceneManager.LoadScene("Lake");
        }
    }
}
