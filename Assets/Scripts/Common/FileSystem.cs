﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Assets.Scripts.Player.Persistence;

namespace Assets.Scripts.Common
{

    public class FileSystem
    {
        private const string DataPath = @"./data/";
        private const string StatsFileName = @"player.xml";

        static string DataFilePath(string fileName) => DataPath + fileName;
        
        public static void SaveStats(PlayerStats data) =>
            SaveData(data, StatsFileName);
        public static PlayerStats LoadStats() =>
            LoadData<PlayerStats>(StatsFileName);

        static void ValidatePath(string fileName)
        {
            var filePath = DataFilePath(fileName);

            if (!Directory.Exists(DataPath))
                Directory.CreateDirectory(DataPath);

            if (!File.Exists(filePath))
            {
                var file = File.Create(filePath);
                file.Close();
            }
        }

        static void SaveData<T>(T data, string fileName)
        {
            ValidatePath(fileName);
            using (var sw = new StreamWriter(DataFilePath(fileName)))
            {
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(sw, data);
                sw.Flush();
            }
        }

        static T LoadData<T>(string fileName)
        {
            try
            {
                T result;
                ValidatePath(fileName);
                using (var fs = File.Open(DataFilePath(fileName), FileMode.Open))
                {
                    var serializer = new XmlSerializer(typeof(T));
                    result = (T)serializer.Deserialize(fs);
                }

                return result;
            }
            catch
            {
                return default(T);
            }
        }
    }
}
