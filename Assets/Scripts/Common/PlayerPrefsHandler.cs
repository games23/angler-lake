﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Common
{
    public static class PlayerPrefsHandler
    {
        public static string GetPlayerName => PlayerPrefs.GetString("playerName");
        public static string GetPlayerId => PlayerPrefs.GetString("playerId");
        public static string GetCountry => PlayerPrefs.GetString("country");
        public static bool IsFirstTimePlaying => PlayerPrefs.GetInt("FTUX") != 1;

        public static void Clear()
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
        }

        public static void SavePlayerId(string guid)
        {
            PlayerPrefs.SetString("playerId", guid);
            PlayerPrefs.Save();
        }

        public static void SavePlayerName(string playerName)
        {
            PlayerPrefs.SetString("playerName", playerName);
            PlayerPrefs.Save();
        }

        public static void SaveCountry(string country)
        {
            PlayerPrefs.SetString("country", country);
            PlayerPrefs.Save();
        }

        public static void SetFTUX() => PlayerPrefs.SetInt("FTUX", 1);
    }
}
