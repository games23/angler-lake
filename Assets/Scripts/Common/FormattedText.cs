﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Common
{
    [RequireComponent(typeof(Text))]
    public class FormattedText : MonoBehaviour
    {
       public string Format;

        public void SetFormattedText(params string[] values) => GetComponent<Text>().text = string.Format(Format, values);
        public void SetFormattedText(params object[] values) => GetComponent<Text>().text = string.Format(Format, values);
    }
}
