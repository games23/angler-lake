﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Common.UI
{
    public class PauseMenu : MonoBehaviour
    {
        public PopUpPanel PopUp;
        public Selectable FirstItem;

        private Action onClose;

        public void Initialize(Action onClose)
        {
            gameObject.SetActive(false);
            this.onClose = onClose;
        }

        public void Show()
        {
            gameObject.SetActive(true);
            FirstItem.Select();
            PopUp.Open();
        }

        public void ExitLake() => SceneManager.LoadScene("MainMenu");

        void Update()
        {
            if (Input.GetButtonDown("Cancel") || Input.GetButtonDown("Options"))
                Close();
        }

        public void Close() => PopUp.Close(OnClosed);

        void OnClosed()
        {
            gameObject.SetActive(false);
            onClose();
        }
    }
}
