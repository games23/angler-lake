﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Common.UI
{
    public class PopUpPanel : MonoBehaviour
    {
        public Animator Animator;
        public AnimationClip OutClip;
        public AudioSource InSfx;
        public AudioSource OutSfx;

        public void Open() => InSfx.Play();

        public void Close(Action onFinished)
        {
            OutSfx.Play();
            Animator.SetTrigger("Close");
            StartCoroutine(WaitAndDo(OutClip.length, onFinished));
        }

        private IEnumerator WaitAndDo(float time, Action onFinished)
        {
            yield return new WaitForSeconds(time);
            onFinished();
        }
    }
}
