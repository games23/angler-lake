﻿using UnityEngine;

namespace Assets.Scripts.Common.UI
{
    public class Page : MonoBehaviour
    {
        public void Show() => gameObject.SetActive(true);
        public void Hide() => gameObject.SetActive(false);
        public string Title;
    }
}
