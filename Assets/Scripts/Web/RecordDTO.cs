﻿using System;
using Assets.Scripts.Common;
using Assets.Scripts.Fishing;
using Newtonsoft.Json;

namespace Assets.Scripts.Web
{
    [Serializable]
    public class RecordDTO
    {
        [JsonProperty("playerId")]
        public string PlayerId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("fishId")]
        public string FishId { get; set; }

        [JsonProperty("catchDate")]
        public string CatchDate { get; set; }

        [JsonProperty("weight")]
        public float Weight { get; set; }

        public static RecordDTO FromFish(Fish fish)
        {
            return new RecordDTO
            {
                Weight = fish.Stats.Weight,
                Name = PlayerPrefsHandler.GetPlayerName,
                FishId = fish.Stats.Id,
                PlayerId = PlayerPrefsHandler.GetPlayerId,
                Country = PlayerPrefsHandler.GetCountry,
                CatchDate = DateTime.Today.ToString("dd/MM/yy")
            };
        }
    }
}
