﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using com.shephertz.app42.paas.sdk.csharp;
using com.shephertz.app42.paas.sdk.csharp.storage;
using Newtonsoft.Json;
using UnityEngine;

namespace Assets.Scripts.Web
{
    public class App42DataManager
    {
        StorageService storageService;

        public App42DataManager()
        {
            var serviceAPI = new ServiceAPI(StaticConfiguration.App42APIKey, StaticConfiguration.App42APISecret);
            storageService = serviceAPI.BuildStorageService();
        }

        public void AddRecord(RecordDTO recordData)
        {
            var recordJson = JsonConvert.SerializeObject(recordData);
            storageService.InsertJSONDocument(StaticConfiguration.DbName, StaticConfiguration.Collection, recordJson, new AddRankingCallBack());
        }

        public void GetRecords(Action<IEnumerable<RecordDTO>> onDataLoaded) => 
            storageService.FindAllDocuments(StaticConfiguration.DbName, StaticConfiguration.Collection, new GetRankingCallback(onDataLoaded));
    }

    public class AddRankingCallBack : App42CallBack
    {
        public void OnSuccess(object response)
        {
            Debug.Log("data send successful");
            App42Log.Console("data send successful");
        }

        public void OnException(Exception e)
        {
            Debug.LogError(e);
            App42Log.Console("Exception : " + e);
        }
    }

    public class GetRankingCallback : App42CallBack
    {
        private Action<IEnumerable<RecordDTO>> onDataLoaded;

        public GetRankingCallback(Action<IEnumerable<RecordDTO>> onDataLoaded) => this.onDataLoaded = onDataLoaded;

        public void OnSuccess(object response)
        {
            Debug.Log("Received data from App42");
            var storage = (Storage)response;
            var jsonDocList = storage.GetJsonDocList();

            var trophies = 
                jsonDocList.Select(jd => JsonConvert.DeserializeObject<RecordDTO>(jd.jsonDoc)).ToArray();
            onDataLoaded(trophies);
        }

        public void OnException(Exception e)
        {
            App42Log.Console("Exception : " + e);
            Debug.LogError(e);
        }
    }
}
