﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.Scripts.Fishing;

namespace Assets.Scripts.Player
{
    public class FishBucket
    {
        public List<Fish> Catches { get; private set; }

        public FishBucket() => Catches = new List<Fish>();

        public void AddCatch(Fish fish) => Catches.Add(fish);

        public void EmptyBucket() => Catches.Clear();
    }
}
