﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Common;
using Assets.Scripts.Fishing;
using UnityEngine;

namespace Assets.Scripts.Player.Persistence
{
    public static class PlayerPersistence
    {
        public static PlayerStats PlayerStats;

        public static void SaveStats() => FileSystem.SaveStats(PlayerStats);
        public static void LoadStats() => PlayerStats = FileSystem.LoadStats();

        public static void AddCatches(IEnumerable<Fish> bucket)
        {
            PlayerStats.BluegillCatches += bucket.Count(f => f.Stats.Id.Equals("bluegill"));
            PlayerStats.WalleyeCatches += bucket.Count(f => f.Stats.Id.Equals("walleye"));
            PlayerStats.WhitefishCatches += bucket.Count(f => f.Stats.Id.Equals("whitefish"));
            PlayerStats.BTroutCatches += bucket.Count(f => f.Stats.Id.Equals("bull-trout"));
            PlayerStats.GTroutCatches += bucket.Count(f => f.Stats.Id.Equals("golden-trout"));
            PlayerStats.MuskieCatches += bucket.Count(f => f.Stats.Id.Equals("tiger-muskie"));
            PlayerStats.PikeCatches += bucket.Count(f => f.Stats.Id.Equals("northern-pike"));
            PlayerStats.CarpCatches += bucket.Count(f => f.Stats.Id.Equals("carp"));
            PlayerStats.BassCatches += bucket.Count(f => f.Stats.Id.Equals("largemouth-bass"));

            var bucketIds = bucket.Select(f => f.Stats.Id).Distinct();
            var bestCatches = new List<Fish>();

            foreach (var id in bucketIds)
            {
                var bestCatch = bucket.Where(f => f.Stats.Id.Equals(id)).OrderByDescending(f => f.Stats.Weight).First();
                bestCatches.Add(bestCatch);
            }

            var trophies = PlayerStats.Trophies;
            var newTrophies = new List<TrophyFish>();
            foreach (var f in bestCatches)
            {
                if (trophies != null && trophies.Any(t => t.Id.Equals(f.Stats.Id)))
                {
                    var currentTrophy = trophies.First(t => t.Id.Equals(f.Stats.Id));
                    if (f.Stats.Weight > currentTrophy.Weight)
                        newTrophies.Add(TrophyFish.FromFish(f));
                    else
                        newTrophies.Add(currentTrophy);
                }
                else
                    newTrophies.Add(TrophyFish.FromFish(f));
            }

            newTrophies = trophies!= null ? newTrophies.Union(trophies.Where(t => !bucketIds.Contains(t.Id))).ToList() : newTrophies;
            PlayerStats.Trophies = newTrophies.ToArray();
        }
    }
}
