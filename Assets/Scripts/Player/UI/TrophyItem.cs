﻿using Assets.Scripts.Common;
using Assets.Scripts.Fishing;
using UnityEngine;

namespace Assets.Scripts.Player.UI
{
    public class TrophyItem : MonoBehaviour
    {
        public FormattedText Text;

        public void InsertData(string name, Tier tier, float weight) => 
            Text.SetFormattedText(name, tier.ToString(), weight.ToString("0.00"));
    }
}
