﻿using Assets.Scripts.Player.Persistence;
using UnityEngine;

namespace Assets.Scripts.Player.UI
{
    public class TrophiesPage : MonoBehaviour
    {
        public Transform Container;
        public TrophyItem TrophyItemPrefab;

        public void FillData(PlayerStats data)
        {
            var oldItems = Container.GetComponentsInChildren<TrophyItem>();
            foreach (var i in oldItems)
                Destroy(i.gameObject);

            if (data.Trophies != null)
            {
                foreach (var t in data.Trophies)
                {
                    var item = Instantiate(TrophyItemPrefab, Container);
                    item.InsertData(t.Name, t.Tier, t.Weight);
                }
            }
        }

        public void Hide() => gameObject.SetActive(false);
        public void Show() => gameObject.SetActive(true);
    }
}
