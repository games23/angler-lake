﻿using Assets.Scripts.Common;
using Assets.Scripts.Player.Persistence;
using UnityEngine;

namespace Assets.Scripts.Player.UI
{
    public class CatchesPage : MonoBehaviour
    {
        public FormattedText TotalCatches;
        public FormattedText Carp;
        public FormattedText Pike;
        public FormattedText BTrout;
        public FormattedText GTrout;
        public FormattedText Muskie;
        public FormattedText Whitefish;
        public FormattedText Bluegill;
        public FormattedText Bass;
        public FormattedText Walleye;

        public void FillData(PlayerStats data)
        {
            TotalCatches.SetFormattedText(data.TotalCatches);
            Carp.SetFormattedText(data.CarpCatches);
            Pike.SetFormattedText(data.PikeCatches);
            BTrout.SetFormattedText(data.BTroutCatches);
            GTrout.SetFormattedText(data.GTroutCatches);
            Muskie.SetFormattedText(data.MuskieCatches);
            Whitefish.SetFormattedText(data.WhitefishCatches);
            Bluegill.SetFormattedText(data.BluegillCatches);
            Bass.SetFormattedText(data.BassCatches);
            Walleye.SetFormattedText(data.WalleyeCatches);
        }

        public void Hide() => gameObject.SetActive(false);
        public void Show() => gameObject.SetActive(true);
    }
}
