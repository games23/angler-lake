﻿using System;
using Assets.PlayerCharacter;
using Assets.Scripts.Common;
using Assets.Scripts.Common.UI;
using Assets.Scripts.Fishing;
using Assets.Scripts.Pier.UI;
using Assets.Scripts.Player.Persistence;
using Assets.Scripts.Player.UI;
using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInitializer : MonoBehaviour
    {
        public Character Character;
        public PlayerInput PlayerInput;
        public FishingDirector FishingDirector;
        public FormattedText BaitCount;
        public Pier.Pier Pier;
        public LicenseInputPanel LicensePanel;
        public PlayerStatsPanel PlayerStatsPanel;
        public PauseMenu PauseMenu;
        public AssistPanel AssistPanel;
        public RecordContainer RankingPanel;

        private FishBucket bucket;
        private bool isDocked;
        
        void Start()
        {
            PlayerPersistence.LoadStats();
            bucket = new FishBucket();
            PlayerStatsPanel.Initialize(ResumeControl);
            PauseMenu.Initialize(ResumeControl);
            RankingPanel.Initialize(ResumeControl);
            Character.Initialize(AssistPanel, FishingDirector, BaitCount.SetFormattedText);
            PlayerInput.Initialize(Character, OpenPlayerStats, OpenMenu, OpenRanking);
            FishingDirector.Initialize(PlayerInput, Character, bucket.AddCatch);

            AssistPanel.HideRankingAssist();
            Pier.Initialize(Dock, UnDock, bucket);

            if (!PlayerPrefs.HasKey("playerId"))
            {
                PlayerPrefsHandler.SavePlayerId(Guid.NewGuid().ToString());
                LicensePanel.Show(SavePlayerData);
                PlayerInput.Disable();
            }
            else
                LicensePanel.Hide();
        }

        private void OpenRanking()
        {
            if (isDocked)
            {
                RankingPanel.Show();
                PlayerInput.Disable();
            }
        }

        private void OpenMenu()
        {
            PauseMenu.Show();
            PlayerInput.Disable();
        }

        private void ResumeControl() => PlayerInput.Enable();

        void SavePlayerData(string playerName, string country)
        {
            PlayerPrefsHandler.SavePlayerName(playerName);
            PlayerPrefsHandler.SaveCountry(country);
            LicensePanel.Hide();
            ResumeControl();
        }

#if UNITY_EDITOR
        [MenuItem("Player Prefs/Clear")]
        static void ClearPlayerPrefs() => PlayerPrefsHandler.Clear();
#endif

        void Dock()
        {
            isDocked = true;
            PlayerPersistence.AddCatches(bucket.Catches);
            PlayerPersistence.SaveStats();
            Character.ResetBait();
            AssistPanel.ShowRankingAssist();
        }

        void UnDock()
        {
            isDocked = false;
            AssistPanel.HideRankingAssist();
        }

        void OpenPlayerStats()
        {
            PlayerStatsPanel.Show(PlayerPersistence.PlayerStats);
            PlayerInput.Disable();
        }
    }
}
