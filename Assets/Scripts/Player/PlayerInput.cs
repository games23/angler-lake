﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Assets.PlayerCharacter;
using UnityEngine;

namespace Assets.Scripts.Player
{
    public class PlayerInput : MonoBehaviour
    {
        BoatMovement boat;
        CursorMovement cursor;
        Character character;

        private Action openStats;
        private Action openOptions;
        private Action openRaking;

        public void Initialize(Character character, Action openStats, Action openOptions, Action openRaking)
        {
            this.openOptions = openOptions;
            this.openRaking = openRaking;
            this.openStats = openStats;
            this.character = character;
            boat = character.BoatMovement;
            cursor = character.CursorMovement;
        }

        void Update()
        {
            var moveVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

            boat.Move(moveVector);
            cursor.Move(moveVector);

            if(Input.GetButtonDown("SwitchMode"))
                character.SwitchMode();

            if (Input.GetButtonDown("Action"))
                character.Action();

            if (Input.GetButtonDown("Options"))
                openOptions();

            if (Input.GetButtonDown("Stats"))
                openStats();

            if (Input.GetButtonDown("Ranking"))
                openRaking();
        }

        public void Disable() => enabled = false;
        public void Enable() => enabled = true;
    }
}
