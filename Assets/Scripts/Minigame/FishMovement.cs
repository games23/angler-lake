﻿using System;
using System.Collections;
using Assets.Scripts.Common;
using Assets.Scripts.Fishing;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Minigame
{
    public class FishMovement : MonoBehaviour
    {
        float minWaitTime;
        float maxWaitTime;

        float movementSpeed;
        private Vector2 target;
        private Fish fish;
        private Action onFishChanged;

        public void Initialize(Fish fish, Action onFishChanged)
        {
            this.onFishChanged = onFishChanged;
            StopAllCoroutines();
            this.fish = fish;
            CalculateTimeLimits(fish.Stats.ActivityFactor);
            ChangeZone();
        }

        private void CalculateTimeLimits(float activityFactor)
        {
            minWaitTime = activityFactor/2;
            maxWaitTime = activityFactor;
        }

        void Update() => AddRotation();

        private void AddRotation()
        {
            var stepRotation = Vector3.Lerp(transform.up, target, movementSpeed).normalized;
            transform.eulerAngles = MathHelper.EulerFromDirection(stepRotation);
        }

        void ChangeZone()
        {
            target = Random.insideUnitCircle.normalized;
            movementSpeed = Random.Range(0, fish.Stats.MaxMoveSpeed);
            var time = Random.Range(minWaitTime, maxWaitTime);
            onFishChanged();

            StartCoroutine(WaitAndDo(time, ChangeZone));
        }

        IEnumerator WaitAndDo(float time, Action onWaited)
        {
            yield return new WaitForSeconds(time);
            onWaited();
        }

        public void Stop() => StopAllCoroutines();

    }
}
