﻿using UnityEngine;

namespace Assets.Scripts.Fishing
{
    [CreateAssetMenu(fileName = "Fish", menuName = "ScriptableObjects/Create", order = 1)]
    public class FishParameters : ScriptableObject
    {
        public string Id;
        public string Name;
        public Sprite Photo;
        public float BaseWeight;
        public float BaseFishZone;
        public float BaseEscapeRate;
        public float BaseCatchRate;
        public float BaseMaxMoveSpeed;
        public float BaseLineDegradation;
        public float BaseActivityFactor;

        public float WeightPerLevel;
        public float FishZonePerLevel;
        public float EscapeRatePerLevel;
        public float CatchRatePerLevel;
        public float MaxMoveSpeedPerLevel;
        public float LineDegradationPerLevel;
        public float ActivityFactorPerLevel;
    }
}
