﻿using System;
using System.Linq;
using Assets.Scripts.Common;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Fishing
{
    public class FishingZone : MonoBehaviour
    {
        public FishParameters[] Fishes;
        public WeightedFishTier[] FishTierDistribution;
        public TierWeightRange[] TierWeightRanges;

        WeightedList<FishTier> weightedFishTiers;

        private bool IsFirstTimePlaying => PlayerPrefsHandler.IsFirstTimePlaying;

        public void Regenerate()
        {
            RandomizeWeights();
            weightedFishTiers = new WeightedList<FishTier>(FishTierDistribution);
        }

        public Fish GetNewFish()
        {
            if (IsFirstTimePlaying)
                return new Fish(0, Tier.Common, Fishes.First(fp => fp.Id.Equals("whitefish")));
            else
            {
                var tier = weightedFishTiers.GetRandomItem();
                var level = tier.RandomLevel;
                return new Fish(level, tier.Tier, Fishes.PickOne());
            }
        }

        void RandomizeWeights()
        {
            foreach (var twr in TierWeightRanges)
                FishTierDistribution.First(td => td.Item.Tier == twr.Tier).Weight =
                    Random.Range(twr.MinWeight, twr.MaxWeight);
        }
    }

    [Serializable]
    public class WeightedFishTier : WeightedItem<FishTier> { }

    [Serializable]
    public class TierWeightRange
    {
        public Tier Tier;
        public int MinWeight;
        public int MaxWeight;
    }
}
