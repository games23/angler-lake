﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Fishing
{
    [Serializable]
    public class FishingDirectorTweaker
    {
        public bool Enabled;
        public string FishId;
        public float Level;
    }
}
