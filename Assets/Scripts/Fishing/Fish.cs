﻿namespace Assets.Scripts.Fishing
{
    public class Fish
    {
        public FishStats Stats;
        public Tier Tier;

        public Fish(float level, Tier tier, FishParameters parameters)
        {
            Tier = tier;

            Stats = FishStats.Create(
                parameters.Id,
                parameters.Name,
                parameters.Photo,
                parameters.BaseWeight + parameters.WeightPerLevel * level,
                parameters.BaseFishZone - parameters.FishZonePerLevel * level,
                parameters.BaseEscapeRate + parameters.EscapeRatePerLevel * level,
                parameters.BaseCatchRate - parameters.CatchRatePerLevel * level,
                parameters.BaseMaxMoveSpeed + parameters.MaxMoveSpeedPerLevel * level,
                parameters.BaseLineDegradation + parameters.LineDegradationPerLevel * level,
                parameters.BaseActivityFactor - parameters.ActivityFactorPerLevel * level
            );
        }
    }
}
